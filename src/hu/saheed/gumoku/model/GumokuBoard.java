
package hu.saheed.gumoku.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GumokuBoard {//extends HashMap<String, Stone> {

//	private int capacity = 100;
	private int numRows = 10;
	private int numCols = 10;

	private static final long serialVersionUID = 1L;
	private int playsX = 0;
	private int playsO = 0;
	
	private Map<String, ArrayList<Stone>> stones = new HashMap<String, ArrayList<Stone>>();
		
	public int size() { 
		int size =0;
		for (ArrayList<Stone> x : stones.values()) {
			size+=x.size();
		}
		return size; 
		}
	
	public boolean isEmpty() { return stones.isEmpty(); }
	
	public boolean containsKey(String k) { return stones.containsKey(k); }
	
	public ArrayList<Stone> get(String player){// throws ElementDoesNotExistException{
		ArrayList<Stone> values = stones.get(player);	
		if (values == null) {	
			values =  new ArrayList<Stone>();	
		}
		return values;
	}
	
	public void put(String key, Stone value) {
		
		ArrayList<Stone> values = stones.get(key);
		if (values == null) {
			ArrayList<Stone> vals = new ArrayList<Stone>();
			vals.add(value);
			stones.put(key, vals);
		}
		else { stones.get(key).add(value);}
	}
	
	public ArrayList<Stone> remove(Stone stone){
		ArrayList<Stone> values = null;
		String player = stone.getPlayer();
		values = stones.get(player).remove(stone)? stones.get(player):null;
		return values;
	}
	
	public void clear() {
	    stones.clear();
	}
	
	public int getCapacity() {
		return numCols * numRows;
	}

	public int getNumCols() {
		return numCols;
	}
	
	public int getNumRows() {
		return numRows;
	}

	public void incrementPlays(String player) {
		if ("X".equals(player)) {
			playsX++;
		} else if ("O".equals(player))  {
			playsO++;
		}
	}

	public int getPlays(String player) {
		if ("X".equals(player)) {
			return playsX;
		} else if ("O".equals(player))  {
			return playsO;
		}
		else return 0;
	}
	
	public boolean isFull() {
		return (this.size() >= getCapacity());
	}


}
