
package hu.saheed.gumoku.model;

import javafx.scene.effect.Light.Point;
import javafx.util.Pair;

public class Stone {//extends Pair<Integer, Integer> {
	private String player;
	private int colCoord;
	private int rowCoord;
	
	public Stone(String player, int col, int row){
		//super(x_,y_);
		this.player = player;
		colCoord = col;
		rowCoord = row;		
	}
	
	@Override
	public String toString() {
		return " [" + colCoord + "," + rowCoord + "]";
	}

	public int[] getCoords(){return new int[]{colCoord,rowCoord};} //new Point(colCoord,rowCoord, 0, null);}//
	public String getPlayer(){return player;}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((player == null) ? 0 : player.hashCode());
		result = prime * result + colCoord;
		result = prime * result + rowCoord;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stone other = (Stone) obj;
		if (player == null) {
			if (other.player != null)
				return false;
		} else if (!player.equals(other.player))
			return false;
		if (colCoord != other.colCoord)
			return false;
		if (rowCoord != other.rowCoord)
			return false;
		return true;
	}
	
	public Pair<Integer, Integer> diff(Stone stone){
		return new Pair<Integer, Integer>(colCoord - stone.colCoord, rowCoord - stone.colCoord);
	}

}
