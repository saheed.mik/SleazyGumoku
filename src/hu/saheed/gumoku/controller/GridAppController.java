

package hu.saheed.gumoku.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import hu.saheed.gumoku.model.GumokuBoard;
import hu.saheed.gumoku.model.Stone;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.fxml.FXML;

import javafx.scene.Node;

import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;

/**
 * @author Saheed
 *
 */
public class GridAppController {

	private GumokuBoard gumokuData = null;
	private boolean gameOn = true;
	private String currPlayer = "X";
	@FXML BorderPane boardGrid;
	@FXML Label statusBar;
	

	private GridPane gridGameBoard = null;

	public GridAppController() {
		gumokuData = new GumokuBoard();
		gridGameBoard = new GridPane();
	}

	@FXML
	private void initialize() {
		boardGrid.getStyleClass().add("gridpane");
		createRootContents(gridGameBoard);					
		boardGrid.setCenter(gridGameBoard);
		//startGame(); - Teacher says Start fresh let me choose, that's fair
		showDialogAlert(AlertType.INFORMATION, "Hi, This is a Sleazy Kind of Gumoku Game !!!" + "\n" + 
												"You can only play in 2 Player mode using a mouse " + "\n" + 
												"Player \"X\" - LEFT Mouse button " + "\n" + 
												"Player \"O\" - RIGHT Mouse button " + "\n" + 
												"Thanks for stopping by, choose OK to continue.");

	}	

	private void createRootContents(GridPane gridBoard) {
		
		int numRows = gumokuData.getNumRows();
		int numCols = gumokuData.getNumCols();
		for (int row = 0; row < numCols; row++) {
		    for (int col = 0; col < numRows; col++) {
		        Button tile = new Button();
		        tile.setPrefSize(65, 65);
//		        tile.setText("");
//		        tile.setOnMouseReleased(eventHandlerBTN);
		        
		        tile.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> { 
					processButtonClick(event);});
		        gridBoard.add(tile, col, row );
		        
		    }
		}
		
	}

	public void processButtonClick(MouseEvent event) {
		Node source = ((Node) event.getSource());

		if (gameOn) {
			if (event.getButton() == MouseButton.PRIMARY) {
				if ("X".equals(currPlayer)) {
					play(source);
					currPlayer = "O";
				}

			} else if (event.getButton() == MouseButton.SECONDARY) {
				if ("O".equals(currPlayer)) {
					play(source);
					currPlayer = "X";
				}
			}
		}
	}

	private void declareWin() {
		
		gumokuData = new GumokuBoard();
		gridGameBoard.getChildren().forEach((node) -> {
			((Button) node).setText(" ");
			((Button) node).setDisable(false);
		});

		updateStatusBar("NEW GAME");
		showDialogAlert(AlertType.INFORMATION, "CONGRATULATIONS !!!" + "\n" + "Player " + currPlayer
				+ " has won this game session." + "\n" + "The Game has restarted, choose OK to continue.");

		//startGame();
		
	}
	
	private void checkForFullBoard(){

		if (gumokuData.isFull()) {
			System.out.println("------Checking Over Here ---");
			showDialogAlert(AlertType.INFORMATION,
					"Hmmmm, Gumoku Board is full." + "\n" + "The Game will now close." + "Thank you.");
			Platform.exit();
		} 
	}

	public void startGame() {
		currPlayer = "X";
		int randomInt = getRandomInt(0, gumokuData.getCapacity()-1);

		Node startTile = gridGameBoard.getChildren().get(randomInt);
		Event.fireEvent(startTile, new MouseEvent(MouseEvent.MOUSE_CLICKED, 0, 0, 0, 0, MouseButton.PRIMARY, 1, true,
				true, true, true, true, true, true, true, true, true, null));
	}

	private int getRandomInt(int min, int max, int... notValidNum) {
		Random rand = new Random();
		return rand.nextInt((max - min) + 1) + min;
	}

	private String getSecondPlayer(String player) {
		return ("X".equals(player) ? "O" : "X");
	}

	private void play(Node source) {
		int colIndex = GridPane.getColumnIndex(source).intValue();
		int rowIndex = GridPane.getRowIndex(source).intValue();
		boolean removeStones = false;

		((Button) source).setText(currPlayer);
		source.setDisable(true);
		Stone currStone = new Stone(currPlayer, colIndex, rowIndex);
		gumokuData.put(currPlayer, currStone);
		gumokuData.incrementPlays(currPlayer);

//		processStone(, currStone);
		 
		boolean	done = horizStonesCheck(gumokuData.get(currPlayer), currStone);
		if(!done) done = vertStonesCheck(gumokuData.get(currPlayer), currStone);
		if(!done) done = diag1_StonesCheck(gumokuData.get(currPlayer), currStone);
		if(!done) done = diag2_StonesCheck(gumokuData.get(currPlayer), currStone);
		if(!done) done = diag3_StonesCheck(gumokuData.get(currPlayer), currStone);
		if(!done) done = diag4_StonesCheck(gumokuData.get(currPlayer), currStone);
		
		/*
//		int unbroken = checkUnbroken(gumokuData.get(currPlayer));
		
		List<Stone> unbrokenStones = getUnbrokenStones(gumokuData.get(currPlayer));
		int unbroken = unbrokenStones.size();
		
		
		
//		if(unbrokenStones.contains(currStone)) removeStones = true;
		for (Stone stone : unbrokenStones) {
			int[] coord = stone.getCoords();
			if((coord[0] == colIndex) & (coord[1] == rowIndex)){
				removeStones = true;
				break;
			}
		}

		showDialogAlert(AlertType.INFORMATION,
				"SIZE OF UNBROKEN IS :  " + unbroken + "\n" +
				"placed stone should be at (" + colIndex + "," + rowIndex + ")" +"\n" +
						"Raw Player Stones : - " + gumokuData.get(currPlayer).toString() +"\n" +
						"Unbrokens :" + unbrokenStones.toString());
		switch (unbroken) {
		case 3:
			if(removeStones) removeStoneRandomly(1);
//			showDialogAlert(AlertType.INFORMATION,
//					"You have Played with " + unbroken + " unbroken stones. Your stones have beem adjusted");
			break;
		case 4:
			if(removeStones) removeStoneRandomly(2);
//			showDialogAlert(AlertType.INFORMATION,
//					"You have Played with " + unbroken + " unbroken stones. Your stones have beem adjusted");
			break;
		case 5:
			declareWin();
			break;
		default:
			break;
		}
*/
		checkForFullBoard();
		
		updateStatusBar("Total plays by player " + currPlayer + " is " + gumokuData.getPlays(currPlayer)
				+ "; Number of stones : " + gumokuData.get(currPlayer).size() + ".  IT'S NOW THE TURN OF PLAYER - "
				+ getSecondPlayer(currPlayer));

	}

	private Node badStone;

	/**
	 * Removes a random stone based on the amount of unbroken stone -- from Data
	 * Model -- clear the stone off the board
	 */
	private void removeStoneRandomly(int numStonesToDel) {
		badStone = null;
		for (int i = 0; i < numStonesToDel; i++) {
			List<Stone> stones = gumokuData.get(currPlayer);
			int index = new Random().nextInt(stones.size());
			Iterator<Stone> iter = stones.iterator();
			for (int it = 0; it < index; it++) {
				iter.next();
			}
			Stone stone = iter.next();
			gumokuData.remove(stone);
			int[] bads = stone.getCoords();
			gridGameBoard.getChildren().forEach(node -> {
				if (GridPane.getColumnIndex(node).intValue() == bads[0]
						&& GridPane.getRowIndex(node).intValue() == bads[1]) {
					badStone = node;
				}
			});
			((Button) badStone).setText("");
			((Button) badStone).setDisable(false);

		}
	}

	
/////////////////////////////////////////////////////////////////////////////////////////////////	
	
	
	
	private boolean vertStonesCheck(List<Stone> stones, Stone newStone){

		List<Stone> lineStones = new ArrayList<Stone>();
		
		for (int col = 0; col < gumokuData.getNumCols(); col++) {
			lineStones.clear();
			for (int row = 0; row < gumokuData.getNumRows(); row++){
				//if (stones.contains(new Stone(currPlayer,row,col))) {
				Stone stn = stonesHas(stones,new Stone(currPlayer,col,row), 0);	
				
				if (stn != null ) {
					lineStones.add(stn);
				}
				else {//detects a break
					if (lineStones.size() >2 && processLine(newStone, lineStones)) {						
						return true;
					}
					lineStones.clear();
				}
				//unbroken = getUnbroken(stones, unbroken, col, row, 0);
				
			}
			if (lineStones.size() >2 && processLine(newStone, lineStones)) {				
				return true;
			}			
		}		
		return false;
	}	

	private boolean horizStonesCheck(List<Stone> stones, Stone newStone){
		
		List<Stone> lineStones = new ArrayList<Stone>();
		
		for (int row = 0; row < gumokuData.getNumRows(); row++) {
			lineStones.clear();
			for (int col = 0; col < gumokuData.getNumCols(); col++) {
				//if (stones.contains(new Stone(currPlayer,row,col))) {
				Stone stn = stonesHas(stones,new Stone(currPlayer,col,row), 0);	
				
				if (stn != null) {
					lineStones.add(stn);
				}
				else {//detects a break
					if (lineStones.size() >2 && processLine(newStone, lineStones)) {
						//false means line not processed
						return true;
					}
					lineStones.clear();
				}
			}
			if (lineStones.size() >2 && processLine(newStone, lineStones)) {
				return true;
			}
		}
		
		return false;
	}
	
	private boolean diag1_StonesCheck(List<Stone> stones, Stone newStone){
		List<Stone> lineStones = new ArrayList<Stone>();
		
		for( int rowStart = 0; rowStart < gumokuData.getNumRows(); rowStart++){
			if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;
			lineStones.clear();
		    int row, col;
		    for( row = rowStart, col = 0; row < gumokuData.getNumRows() 
		    						   && col < gumokuData.getNumCols(); row++, col++ ){
		    	
		    	//TODO This is List.contains !!! 
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		lineStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
					lineStones.clear();
		    	}
		    }
		}
		return false;
	}
	
	private boolean diag2_StonesCheck(List<Stone> stones, Stone newStone){

		List<Stone> lineStones = new ArrayList<Stone>();
		
		for( int colStart = 1; colStart < gumokuData.getNumCols(); colStart++){
			if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
			lineStones.clear();
		    int row, col;
		    for( row = 0, col = colStart; row < gumokuData.getNumRows() && col < gumokuData.getNumCols(); row++, col++ ){
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		lineStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
					lineStones.clear();
		    	}
		    }
		}
		return false;
	}

	private boolean diag3_StonesCheck(List<Stone> stones, Stone newStone){
		
		List<Stone> lineStones = new ArrayList<Stone>();
				
		for( int rowStart = gumokuData.getNumRows()-1; rowStart >= 0; rowStart--){
			if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
			lineStones.clear();
		    int row, col;
		    for( row = rowStart, col = 0 ; row >=0 
		    	   					   && col < gumokuData.getNumCols(); row--, col++ ){
		    	
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		lineStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
					lineStones.clear();
		    	}
		    }
		}
		return false;
	}
	
	private boolean diag4_StonesCheck(List<Stone> stones, Stone newStone){

		List<Stone> lineStones = new ArrayList<Stone>();
		
		for( int colStart = 1; colStart < gumokuData.getNumCols(); colStart++){
			if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
			lineStones.clear();
		    int row, col;
		    for( row = gumokuData.getNumRows()-1, col = colStart; row >=0 
		    		&& col < gumokuData.getNumCols(); row--, col++ ){
		    	
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		lineStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (lineStones.size() >2 && processLine(newStone, lineStones)) return true;    		
					lineStones.clear();
		    	}
		    }
		}
		return false;
	}

	private boolean processLine(Stone newStone, List<Stone> line) {

		if(line.size() >= 5) { //there a win here
			declareWin();
			return true; 
		}else {
			for (Stone stone : line) {
				if(stone.getCoords()[0] == newStone.getCoords()[0] & 
						stone.getCoords()[1] == newStone.getCoords()[1]){

					removeStoneRandomly(line.size() - 2);
					return true;
				}
			}

		}
		return false;
	}

	
	///Returns unbroken stone arrangement > 2 
	private void processStone(List<Stone> stones, Stone newStone){
	
	//getting list of lines @ vertical
	List<List<Stone>> masterList = new ArrayList<List<Stone>>();
	List<Stone> copyStones = new ArrayList<Stone>(stones);			
	while (copyStones.iterator().hasNext()) {
		List<Stone> lineStones = vertStonesUnbroken(copyStones);
		if(lineStones.size() == 0) break;
		masterList.add(lineStones);
		copyStones.removeAll(lineStones);
	}
	
	//check masterList ? return : continue
	if(checkMasterList(newStone, masterList)) return;
	
	//getting list of lines @ horizontal
	masterList.clear();
	copyStones = new ArrayList<Stone>(stones);			
	while (copyStones.iterator().hasNext()) {
		List<Stone> lineStones = horizStonesUnbroken(copyStones);
		if(lineStones.size() == 0) break;
		masterList.add(lineStones);
		copyStones.removeAll(lineStones);
	}

	// (check masterList) ? return : continue
	if(checkMasterList(newStone, masterList)) return;
	
	//getting list of lines @ diagonal 1
	masterList.clear();
	copyStones = new ArrayList<Stone>(stones);
	while (copyStones.iterator().hasNext()) {
		List<Stone> lineStones = diag1_StonesUnbroken(copyStones);
		if(lineStones.size() == 0) break;
		masterList.add(lineStones);
		copyStones.removeAll(lineStones);
	}

	// (check masterList) ? return : continue
	if(checkMasterList(newStone, masterList)) return;
	
	//getting list of lines @ diagonal 2
	masterList.clear();
	copyStones = new ArrayList<Stone>(stones);
	while (copyStones.iterator().hasNext()) {
		List<Stone> lineStones = diag2_StonesUnbroken(copyStones);
		if(lineStones.size() == 0) break;
		masterList.add(lineStones);
		copyStones.removeAll(lineStones);
	}

	// (check masterList) ? return : continue
	if(checkMasterList(newStone, masterList)) return;
	
	//getting list of lines @ diagonal 3
	masterList.clear();
	copyStones = new ArrayList<Stone>(stones);	
	while (copyStones.iterator().hasNext()) {
		List<Stone> lineStones = diag3_StonesUnbroken(copyStones);
		if(lineStones.size() == 0) break;
		masterList.add(lineStones);
		copyStones.removeAll(lineStones);
	}

	// (check masterList) ? return : continue
	if(checkMasterList(newStone, masterList)) return;
	
	//getting list of lines @ diagonal 4
	masterList.clear();
	copyStones = new ArrayList<Stone>(stones);
	while (copyStones.iterator().hasNext()) {
		List<Stone> lineStones = diag4_StonesUnbroken(copyStones);
		if(lineStones.size() == 0) break;
		masterList.add(lineStones);
		copyStones.removeAll(lineStones);
	}
	checkMasterList(newStone, masterList);
	//Case1: Master as nothing => check other sides
	//Case2: Master has =1 element=> check : for 5 => win
									//check: for 4/3 => check if it contains "newStone" => remove
									//check: max <= 2 => check other sides
	
	//Case3: Master has >1 elements => check for max numbered lists = max
								//check: max == 5 => win
								//check: max == 4/3 => check if it contains "newStone" => remove
								//check: max <= 2 => check other sides
			
	}

	private boolean checkMasterList(Stone newStone, List<List<Stone>> masterList) {
	
	if (masterList.size() < 1) {
		return false; //no line & no win here
	} else { // @least 1 line here, is it a win ?; OR ... 
		//get max line in masterList
		//if: max is 5 => implement a win(currPlayer); return true;
		//else: x_masterList = List of lines in masterList with length max
		    //: if any line in x_masterList has (currStone) => remove(max-2)
			//: return true
	
		int maxLength = max(masterList.stream().mapToInt(x -> x.size()).toArray());
		if(maxLength >= 5) { //there a win here
			declareWin();
			return true; 
		}else if (maxLength == 3 | maxLength ==4){
			List<List<Stone>> maxLines = new ArrayList<List<Stone>>(); 
			//(ArrayList<List<Stone>>) masterList.stream().filter(x -> x.size() == maxLength);
			
			for (List<Stone> list : masterList) {
				if (list.size() == maxLength) {
					maxLines.add(list);
				}
			}
//			List<Stone> line = maxLines.stream().filter(x -> x.contains(newStone)).findFirst()
//			List<Stone> line =  new ArrayList<Stone>();
//			boolean found = false;
			
			for (List<Stone> ln : maxLines) {
				//				if(found)break;
				for (Stone stone : ln) {
					if(stone.getCoords()[0] == newStone.getCoords()[0] & stone.getCoords()[1] == newStone.getCoords()[1]){

						//found = true; line = ln;  
						removeStoneRandomly(maxLength - 2);
						return true;
					}
				}
			}
			
//			if (line.size() >2) {
//				removeStones(maxLength - 2);
//			} else {
//
//			}
		}
		else return false;
	}
	return false;
}
	
	private List<Stone> vertStonesUnbroken(List<Stone> stones){
		
		List<Stone> strStones = new ArrayList<Stone>();
		
		for (int col = 0; col < gumokuData.getNumCols(); col++) {
			strStones.clear();
			for (int row = 0; row < gumokuData.getNumRows(); row++){
				//if (stones.contains(new Stone(currPlayer,row,col))) {
				Stone stn = stonesHas(stones,new Stone(currPlayer,col,row), 0);	
				
				if (stn != null ) {
					strStones.add(stn);
				}
				else {//detects a break
					if (strStones.size() >2) return strStones;
					strStones.clear();
				}
				//unbroken = getUnbroken(stones, unbroken, col, row, 0);
				
			}
			if (strStones.size() >2) break;
		}
		
		return strStones;
	}
	
	private List<Stone> horizStonesUnbroken(List<Stone> stones){
		
		List<Stone> strStones = new ArrayList<Stone>();
		
		for (int row = 0; row < gumokuData.getNumRows(); row++) {
			strStones.clear();
			for (int col = 0; col < gumokuData.getNumCols(); col++) {
				//if (stones.contains(new Stone(currPlayer,row,col))) {
				Stone stn = stonesHas(stones,new Stone(currPlayer,col,row), 0);	
				
				if (stn != null) {
					strStones.add(stn);
				}
				else {//detects a break
					if (strStones.size() >2) return strStones;
					strStones.clear();
				}
			}
			if (strStones.size() >2) break;
		}
		
		return strStones;
	}
	
	private List<Stone> diag1_StonesUnbroken(List<Stone> stones){
		List<Stone> strStones = new ArrayList<Stone>();
		
		for( int rowStart = 0; rowStart < gumokuData.getNumRows(); rowStart++){
			if (strStones.size() >2) return strStones;    		
			strStones.clear();
		    int row, col;
		    for( row = rowStart, col = 0; row < gumokuData.getNumRows() 
		    						   && col < gumokuData.getNumCols(); row++, col++ ){
		    	
		    	//TODO This is List.contains !!! 
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		strStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (strStones.size() >2) return strStones;    		
					strStones.clear();
		    	}
		    }
		}
		return strStones;
	}
	
	private List<Stone> diag2_StonesUnbroken(List<Stone> stones){

		List<Stone> strStones = new ArrayList<Stone>();
		
		for( int colStart = 1; colStart < gumokuData.getNumCols(); colStart++){
			if (strStones.size() >2) return strStones;    		
			strStones.clear();
		    int row, col;
		    for( row = 0, col = colStart; row < gumokuData.getNumRows() && col < gumokuData.getNumCols(); row++, col++ ){
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		strStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (strStones.size() >2) return strStones;    		
					strStones.clear();
		    	}
		    }
		}
		return strStones;
	}

	private List<Stone> diag3_StonesUnbroken(List<Stone> stones){
		
		List<Stone> strStones = new ArrayList<Stone>();
				
		for( int rowStart = gumokuData.getNumRows()-1; rowStart >= 0; rowStart--){
			if (strStones.size() >2) return strStones;    		
			strStones.clear();
		    int row, col;
		    for( row = rowStart, col = 0 ; row >=0 
		    	   					   && col < gumokuData.getNumCols(); row--, col++ ){
		    	
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		strStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (strStones.size() >2) return strStones;    		
					strStones.clear();
		    	}
		    }
		}
		return strStones;
	}
	
	private List<Stone> diag4_StonesUnbroken(List<Stone> stones){

		List<Stone> strStones = new ArrayList<Stone>();
		
		for( int colStart = 1; colStart < gumokuData.getNumCols(); colStart++){
			if (strStones.size() >2) return strStones;    		
			strStones.clear();
		    int row, col;
		    for( row = gumokuData.getNumRows()-1, col = colStart; row >=0 
		    		&& col < gumokuData.getNumCols(); row--, col++ ){
		    	
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){
		    		strStones.add(new Stone(currPlayer,row,col));
		    		}
		    	else{
		    		if (strStones.size() >2) return strStones;    		
					strStones.clear();
		    	}
		    }
		}
		return strStones;
	}

/////////////////////////////////////////////////////////////////////////////////////////////	

	
	private int numUnbrokenHoriz(List<Stone> stones){
		
		int unbroken = 0;
		
		for (int row = 0; row < gumokuData.getNumRows(); row++) {
			unbroken = 0;
			for (int col = 0; col < gumokuData.getNumCols(); col++) {
				//if (stones.contains(new Stone(currPlayer,row,col))) {
				Stone stn =  new Stone(currPlayer,col,row);				
				if (stonesHas(stones,stn)) {
					unbroken++;
				}
				else {//detects a break
					if (unbroken >2) return unbroken;
					unbroken = 0;
				}
			}
			if (unbroken >2) break;
		}
		
		return unbroken;
	}
	
	//TODO Method is half edited - I had to create a new method for all (NO TIME FOR GOOD CODE #Microsoft)
	private int numUnbrokenVert(List<Stone> stones){
		
		int unbroken = 0;
		List<Stone> strStones = new ArrayList<Stone>();
		
		for (int col = 0; col < gumokuData.getNumCols(); col++) {
			unbroken = 0;
			strStones.clear();
			for (int row = 0; row < gumokuData.getNumRows(); row++){
				//if (stones.contains(new Stone(currPlayer,row,col))) {
				Stone stn =  new Stone(currPlayer,col,row);				
				if (stonesHas(stones,stn)) {
					unbroken++;
					//strStones.add(e)
				}
				else {//detects a break
					if (unbroken >2) return unbroken; //return strStones;
					unbroken = 0;
					strStones.clear();
				}
				//unbroken = getUnbroken(stones, unbroken, col, row, 0);
				
			}
			if (unbroken >2) break;
		}
		
		return unbroken;//return strStones;
	}
	
	private int diagonal_1(List<Stone> stones){
		int count = 0;
		for( int rowStart = 0; rowStart < gumokuData.getNumRows(); rowStart++){
			if(count >2) return count;    		
			count = 0;
		    int row, col;
		    for( row = rowStart, col = 0; row < gumokuData.getNumRows() 
		    						   && col < gumokuData.getNumCols(); row++, col++ ){
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){count++;}
		    	else{
		    		if(count >2) return count;
		    		count = 0;
		    	}
		    }
		}
		return count;
	}
	
	private int diagonal_2(List<Stone> stones){

		int count = 0;
		for( int colStart = 1; colStart < gumokuData.getNumCols(); colStart++){
			if(count > 2) return count;
			count = 0;
		    int row, col;
		    for( row = 0, col = colStart; row < gumokuData.getNumRows() && col < gumokuData.getNumCols(); row++, col++ ){
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){count++;}
		    	else{
		    		if(count >2) return count;
		    		count = 0;
		    	}
		    }
		}
		return count;
	}

	private int diagonal_3(List<Stone> stones){
		int count = 0;

		for( int rowStart = gumokuData.getNumRows()-1; rowStart >= 0; rowStart--){
			if(count >2) return count;    		
			count = 0;
		    int row, col;
		    for( row = rowStart, col = 0 ; row >=0 
		    	   					   && col < gumokuData.getNumCols(); row--, col++ ){
		    	
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){count++;}
		    	else{
		    		if(count >2) return count;
		    		count = 0;
		    	}		    	
		    }
		}
		return count;
	}
	
	private int diagonal_4(List<Stone> stones){

		int count = 0;
		for( int colStart = 1; colStart < gumokuData.getNumCols(); colStart++){
			if(count > 2) return count;
			count = 0;
		    int row, col;
		    for( row = gumokuData.getNumRows()-1, col = colStart; row >=0 && col < gumokuData.getNumCols(); row--, col++ ){
		    	
		    	boolean found = false;
		    	for(Stone s : stones){
		    		if ((s.getCoords()[0] == row)&&(s.getCoords()[1] == col)) {
		    			found = true;
					}
		    	}
		    	if(found){count++;}
		    	else{
		    		if(count >2) return count;
		    		count = 0;
		    	}
		    }
		}
		return count;
	}
	
	private boolean stonesHas(List<Stone> stones, Stone st){
//		System.out.println("*************"); 
		for(Stone s : stones){
//			System.out.println(" looking for (" + st.getCoords()[0] + "," + st.getCoords()[1] + ")");
//			System.out.println(" I am at stone (" + s.getCoords()[0] + "," + s.getCoords()[1] + ")");
			
			if((s.getCoords()[0] == st.getCoords()[0]) &
			   (s.getCoords()[1] == st.getCoords()[1])){
				return true;			
				
			}
		}
		return false;
	}
	
	private Stone stonesHas(List<Stone> stones, Stone st, int nothing){
//		System.out.println("*************"); 
		for(Stone s : stones){
//			System.out.println(" looking for (" + st.getCoords()[0] + "," + st.getCoords()[1] + ")");
//			System.out.println(" I am at stone (" + s.getCoords()[0] + "," + s.getCoords()[1] + ")");
			
			if((s.getCoords()[0] == st.getCoords()[0]) &
			   (s.getCoords()[1] == st.getCoords()[1])){
				return s;			
				
			}
		}
		return null; //TODO Yes !!! I am a good developer (capability to write bad code)- checked
	}

	public int checkUnbroken(List<Stone> stones) {
		
		return Math.max(Math.max(numUnbrokenVert(stones), numUnbrokenHoriz(stones)), //numUnbrokenDiag(stones));
				Math.max(Math.max(diagonal_2(stones), diagonal_1(stones)), 
				Math.max(diagonal_4(stones), diagonal_3(stones))));
		
	}

	public List<Stone> getUnbrokenStones(List<Stone> stones) {
		int vert = vertStonesUnbroken(stones).size();
		int horiz = horizStonesUnbroken(stones).size();
		int diag1 = diag1_StonesUnbroken(stones).size();
		int diag2 = diag2_StonesUnbroken(stones).size();
		int diag3 = diag3_StonesUnbroken(stones).size();
		int diag4 = diag4_StonesUnbroken(stones).size();
		
		/*
		int highest = Math.max(Math.max(vert, horiz),
					  Math.max(Math.max(diag2, diag1), 
					  Math.max(diag4, diag3)));
		*/
		
		int highest = max(new int[]{vert, horiz, diag1, diag2, diag3, diag4});
		
		if(highest == vert) return vertStonesUnbroken(stones);
		else if(highest == horiz) return horizStonesUnbroken(stones);
		else if(highest == diag1) return diag1_StonesUnbroken(stones);
		else if(highest == diag2) return diag2_StonesUnbroken(stones);
		else if(highest == diag3) return diag3_StonesUnbroken(stones);
		else return diag4_StonesUnbroken(stones);
		
//		return vertStonesUnbroken(stones);
		
	}
	
	public int max(int... intergers) {
	    int i = 0;
	    int max = intergers[i];

	    while (++i < intergers.length)
	        if (intergers[i] > max)
	            max = intergers[i];

	    return max;
	}

	private Alert showDialogAlert(AlertType alertType, String msg) {
		Alert alert = new Alert(alertType);
		alert.setTitle("GUMOKU GAME");
		alert.setHeaderText(null);
		alert.setContentText(msg);
		alert.showAndWait();
		return alert;
	}
	
	public void updateStatusBar(String msg) {
		statusBar.setWrapText(true);
		statusBar.setText("STATUS: " + msg);
	}
}
